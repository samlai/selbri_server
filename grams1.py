import sys
import regex as re  # type: ignore
import pickle
from nltk.probability import FreqDist  # type: ignore

if __name__ == '__main__':
    if not sys.flags.inspect:
        from morfeusz2 import Morfeusz  # type: ignore


def _load_1grams(name):
    fr = FreqDist()
    with open(name) as f:
        for i, l in enumerate(f):
            m = l.strip().split()
            if len(m) == 2:
                num = float(m[0])
                word = m[1]
            else:
                print(f"Bad syntax in {i}")
            fr[word] += num
    return fr


_nkjp_1grams = None


def load_nkjp_1grams():
    global _nkjp_1grams
    if not _nkjp_1grams:
        with open("data/1grams.pkl", "rb") as f:
            _nkjp_1grams = pickle.load(f)
    return _nkjp_1grams


_nkjp_1grams_stemed = None


def load_nkjp_1grams_stemed():
    global _nkjp_1grams_stemed
    if not _nkjp_1grams_stemed:
        with open("data/1grams_stemed.pkl", "rb") as f:
            _nkjp_1grams_stemed = pickle.load(f)
    return _nkjp_1grams_stemed


def check_value(name, term):
    with open(name) as f:
        for l in f:
            num, txt = l.strip().split(" ", 1)
            if txt == term:
                return int(num)
    return 0


def check_values(name, terms):
    out = [None] * len(terms)
    with open(name) as f:
        for l in f:
            num, txt = l.strip().split(" ", 1)
            if txt in terms:
                out[terms.index(txt)] = int(num)
                if None not in out:
                    return out

    def zer(n):
        if n == None:
            return 0
        else:
            return n
    return list(map(zer, out))


def ctn(l):
    print(f"ctn({l})")
    if l == []:
        return 1
    n = len(l)
    return check_value(f"data/{n}grams", " ".join(l))


def ctn_n(n, l):
    return check_values(f"data/{n}grams", l)


def find_walent(ver, subts):
    for i in range(2, 6):
        with open(f"data/{i}grams") as f:
            for l in f:
                record = l.strip().split(" ")
                num = record[0]
                words = record[1:]
                fst = words[0]
                lst = words[-1]
                if fst in ver and lst in subts:
                    print(f"record: {l}")
                    yield int(num), words[1:]


def walent_ctn(ver, subst):
    fr = FreqDist()
    for n, wal in find_walent(ver, subst):
        fr[tuple(wal)] += n
    return fr


def steeming_1grams():
    fr = FreqDist()
    with open("1grams") as f:
        morf = Morfeusz(generate=False)
        for l in f:
            m = re.match("^\s*(\d*)\s+(\S*)\s*$", l)
            num = int(m.group(1))
            word = m.group(2)
            S = morf.analyse(word)
            n = len(S)
            for _, _, p in S:
                fr[p[1].split(":")[0]] += num/n

    with open("1grams_stemed", "w") as f:
        for w in fr:
            f.write(f"{fr[w]}\t{w}\n")


if __name__ == '__main__':
    if not sys.flags.inspect:
        if len(sys.argv) > 1 and sys.argv[1] == '--init':
            steeming_1grams()
