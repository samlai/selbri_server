#MODEL="jbopol_zabuzane_mostrandom50_0.25.model"

#python wv_translation.py split $MODEL "jbo_0.wv" "pol_0.wv" >> boot$1.log 

mkdir -p /pio/data/corpuses/boot/$1

#python3 wv_translation.py boot "jbo_0.wv" "pol_0.wv" "$2" "$1/boot_${1}_0.csv" 

#cat "/pio/data/corpuses/boot/$1/boot_0.csv" /pio/data/corpuses/boot/jbo2-pol.train.txt > "/pio/data/corpuses/boot/$1/train_${1}_0.csv"

# train_i.csv --> boot_i.model --> jbo_i.wv, pol_i.wv --> boot_i+1.csv --> train_i+1.csv


# Poprawka:

#cat "/pio/data/corpuses/boot/$1/boot_0.csv" /pio/data/corpuses/boot/jbo2-pol.train.txt > "/pio/data/corpuses/boot/$1/train_${1}_0.csv"

#python wv_translation.py boot "$1/jbo_${1}_0.wv" "$1/pol_${1}_0.wv" "$2" "$1/boot_${1}_1.csv" || exit 1

#cat "/pio/data/corpuses/boot/$1/boot_${1}_1.csv" "/pio/data/corpuses/boot/$1/train_${1}_0.csv" > "/pio/data/corpuses/boot/$1/train_${1}_1.csv" || exit 1

for i in $(seq 3 10)
do
	echo Iteration $1 $i

	python wv_translation.py train 0.5 $3 "$1/train_${1}_$i.csv" "$1/boot_${1}_$i.model"  || exit 1

	python wv_translation.py split "$1/boot_${1}_$i.model" "$1/jbo_${1}_$i.wv" "$1/pol_${1}_$i.wv" || exit 1

	python wv_translation.py boot "$1/jbo_${1}_$i.wv" "$1/pol_${1}_$i.wv" "$2" "$1/boot_${1}_$((i+1)).csv" || exit 1


	cat "/pio/data/corpuses/boot/$1/boot_${1}_$((i+1)).csv" "/pio/data/corpuses/boot/$1/train_${1}_$i.csv" > "/pio/data/corpuses/boot/$1/train_${1}_$((i+1)).csv" || exit 1
done
