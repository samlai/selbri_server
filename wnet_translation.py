from grams1 import load_nkjp_1grams, load_nkjp_1grams_stemed
from plwordnet import load as plwordnet_load  # type: ignore
import regex as re  # type: ignore
from itertools import count
from nltk.corpus import brown  # type: ignore
from nltk.stem.snowball import EnglishStemmer  # type: ignore
from lojban_dictionaries import *
from nltk.probability import FreqDist  # type: ignore
from utils import *
import scoring
import sys
import json


def load_brown_1grams():
    return FreqDist(brown.words())


def load_brown_1grams_steemed():
    en_stem = EnglishStemmer()
    return FreqDist(map(en_stem.stem, brown.words()))


if __name__ == "__main__":
    try:
        wn  # type: ignore
    except NameError:
        print("Load plwordnet!")
        #wn = plwordnet_load('data/plwordnet_4_2/plwordnet_4_2.xml')
        wn = plwordnet_load('data/plwordnet_4_2.pkl')

        print("Load nkjp(1)!")
        nkjp_1grams = load_nkjp_1grams()

        print("Load Mublin Etymology!")
        jbo_etym = load_mublin_eng()

        print("Load Brown!")
        brown_1grams = load_brown_1grams()

    print("Load Brown(2)!")
    brown_1grams_steemed = load_brown_1grams_steemed()
    print("Load nkjp(2)!")
    nkjp_1grams_stemed = load_nkjp_1grams_stemed()


def gizmu_eng_etymology(gizmu):
    return (e[key]
            for e in jbo_etym[gizmu]
            for key in ['English', 'keyword']
            if key in e)


def tlum_eng_pl_0(w):
    """Funkaj przyjmuje słowo angielskie i zwraca ich polskie syninimy między językowe"""
    lemmas = wn.find(w)
    return (c for l in lemmas
            for a, p, c in wn.synset_relations_where(subject=l.synset)
            if p.id == 209)


def gismu_jbo_pl_naive(gizmu: str):
    """Metoda 0: Słowa nie są w żaden dodatkowy sposób układane."""
    return list((0, (u, e))
                for e in gizmu_eng_etymology(gizmu)

                for semn in tlum_eng_pl_0(e)
                for u in semn.lexical_units)


def gismu_jbo_pl_prob(gizmu: str):
    return sorted(mix_fst((abs(nkjp_1grams[u.name] * brown_1grams.N() - e_prob),
                           (u, e))
                          for e in gizmu_eng_etymology(gizmu)
                          for e_prob in [brown_1grams[e] * nkjp_1grams.N()]
                          for semn in tlum_eng_pl_0(e)
                          for u in semn.lexical_units),
                  key=fst)


def gismu_jbo_pl_norm_prob(gizmu: str):
    return sorted(mix_fst((abs(nkjp_1grams_stemed[u.name] * brown_1grams_steemed.N() - e_prob),
                           (u, e))
                          for e in gizmu_eng_etymology(gizmu)
                          for e_prob in [brown_1grams_steemed[e] * nkjp_1grams_stemed.N()]
                          for semn in tlum_eng_pl_0(e)
                          for u in semn.lexical_units),
                  key=fst)

# Błędy:
#  - Jeżeli zrobiliśmy krok w dół to może warto potem zrobić krok w górę?
#  - Powtaża słowa
#  - brak powiązania:
#       "punish" a "karać"
#       "exist" z "istnieć"
#       "sudden" z "nagły" "niespodziewany"
#       "invent" "wynaleźć"

# - brak odpowiedniego słowa w słowniku:
#       "come" -> przychodzić, go i come jako antonimy w Lojban jako relacja
#       jundi - powiązanie z idomem "pay attention to" - zwracać uwagę na coś


# - jakaś dziwna chęć do przeklinania:
#    pierwsze bardziej poprawne tłumaczenie słowa citka to wpierniczać

SYN_AP = 209
#SYNMR_AP = 229
HIPER_AP = 213
HIPO_AP = 211


def relation_points(i):
    if i == SYN_AP:
        return 0
    elif i == HIPER_AP:
        return 10
    elif i == HIPO_AP:
        return 20


def tlum_eng_pl_distance(w):
    lemmas = wn.find(w)
    return ((relation_points(p.id) + l_dist, c)
            for l_dist, l in enumerate(lemmas)
            for a, p, c in wn.synset_relations_where(subject=l.synset)
            if p.id in [SYN_AP, HIPER_AP, HIPO_AP])


def gismu_jbo_pl_distance(gizmu: str):
    return remove_repetitions(
        sorted(
            ((dist + u_dist, (u, e))
             for e in gizmu_eng_etymology(gizmu)
             for dist, semn in tlum_eng_pl_distance(e)
             for u_dist, u in enumerate(semn.lexical_units)),
            key=fst),
        lambda x: x[1][0].name)


def gismu_jbo_pl_prob_more(gizmu: str):
    return sorted(mix_fst((abs(nkjp_1grams[u.name] * brown_1grams.N() - e_prob),
                           (u, e))
                          for e in gizmu_eng_etymology(gizmu)
                          for e_prob in [brown_1grams[e] * nkjp_1grams.N()]
                          for _, semn in tlum_eng_pl_distance(e)
                          for u in semn.lexical_units),
                  key=fst)


def gismu_jbo_pl_norm_prob_more(gizmu: str):
    return sorted(mix_fst((abs(nkjp_1grams_stemed[u.name] * brown_1grams_steemed.N() - e_prob),
                           (u, e))
                          for e in gizmu_eng_etymology(gizmu)
                          for e_prob in [brown_1grams_steemed[e] * nkjp_1grams_stemed.N()]
                          for _, semn in tlum_eng_pl_distance(e)
                          for u in semn.lexical_units),
                  key=fst)


def gismu_jbo_pl_prob_dist(gizmu: str):
    return sorted(mix_fst((abs(nkjp_1grams[u.name] * brown_1grams.N() - e_prob) + 1000 * dist,
                           (u, e))
                          for e in gizmu_eng_etymology(gizmu)
                          for e_prob in [brown_1grams[e] * nkjp_1grams.N()]
                          for dist, semn in tlum_eng_pl_distance(e)
                          for u in semn.lexical_units),
                  key=fst)


def gismu_jbo_pl_norm_prob_dist(gizmu: str):
    return sorted(mix_fst((abs(nkjp_1grams_stemed[u.name] * brown_1grams_steemed.N() - e_prob) + 1000 * dist,
                           (u, e))
                          for e in gizmu_eng_etymology(gizmu)
                          for e_prob in [brown_1grams_steemed[e] * nkjp_1grams_stemed.N()]
                          for dist, semn in tlum_eng_pl_distance(e)
                          for u in semn.lexical_units),
                  key=fst)


def gismu_jbo_pol_conn(w):
    l = gismu_jbo_pl_prob(w)
    if l == []:
        #l = gismu_jbo_pl_distance(w)
        #l = gismu_jbo_pl_prob_more(w)
        #l = gismu_jbo_pl_norm_prob_more(w)
        #l = gismu_jbo_pl_prob_dist(w)
        l = gismu_jbo_pl_norm_prob_dist(w)
    return l


def tabling_gismus(fun):
    d = dict()
    for w in jbo_etym:
        d[w] = fun(w)
    return d


def wnet_translation_(w):
    return [(lex.pos, lex.name)
            for _dis, (lex, _ang) in gismu_jbo_pol_conn(w)]


def load_translations(file_name):
    print("Load gismu translations!")
    with open(file_name) as f:
        d = json.load(f)

        def wnet_tanslation(w):
            return d[w]
        return wnet_tanslation


def type_classyfication(s):
    return list(set(map(lambda w: w.pos, wn.find(s))))


def latex(call):
    tex = """\\begin{figure}[h!]
\\begin{center}
\\begin{tabular}{ c | c | c}
 szablon & Precision & Maximall Recall \\\\
 \\hline
 most50 & %f\\%% & %d\\%% \\\\
 random50 & %f\\%% & %d\\%% \\\\
\\end{tabular}
\\caption{Ocena programu %s}
\\label{fig:%s}
\\end{center}
\\end{figure}"""

    def f(x):
        return list(map(lambda c: c[1][0].name, call(x)))

    most50 = scoring.scoring_method(f, "data/most50.csv")
    most50_max = scoring.max_scoring_method(f, "data/most50.csv")
    rand50 = scoring.scoring_method(f, "data/random50.csv")
    rand50_max = scoring.max_scoring_method(f, "data/random50.csv")
    print(tex % (most50 * 2, most50_max * 2, rand50 * 2, rand50_max * 2,
                 call.__name__.replace("_", "\\_"), call.__name__))


if __name__ == '__main__' and len(sys.argv) > 1:
    if sys.argv[1] == "tabling":
        d = tabling_gismus(wnet_tanslation_)
        out = json.dumps(d)
        with open(sys.argv[2], "w") as f:
            f.write(out)
    elif sys.argv[1] == "help":
        print(f"{sys.argv[0]} tabling [file]")
        print("""
Create a file with the proposed translations of all gismus.

Warning: You need 3GB of RAM to run tabling!
        """)
else:
    wnet_translation = load_translations("data/gismu_translations.json")
