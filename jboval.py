from subprocess import Popen, PIPE, DEVNULL
from typing import *
import os
import fcntl
import signal
import json
import threading
import signal


signal.signal(signal.SIGCHLD, signal.SIG_IGN)


class Jboval:
    def __init__(self):
        self._lock = threading.Lock()
        self._process = Popen(["./jboval"],
                              stdin=PIPE,
                              stdout=PIPE)

    def vlalei(self, lujvo):
        self._lock.acquire()
        self._process.stdin.write(f"vlalei {lujvo}\n".encode("utf-8"))
        self._process.stdin.flush()
        out = self._process.stdout.readline().decode("utf-8").strip()
        self._lock.release()
        return out

    def terjvo(self, lujvo):
        self._lock.acquire()
        self._process.stdin.write(f"terjvo {lujvo}\n".encode("utf-8"))
        self._process.stdin.flush()
        l = self._process.stdout.readline().decode("utf-8").strip()
        self._lock.release()
        return json.loads(l)

    def rafyjongau(self, rafsis: List[str]) -> str:
        args = " ".join(rafsis)
        self._lock.acquire()
        self._process.stdin.write(f"rafyjongau {args}\n".encode("utf-8"))
        self._process.stdin.flush()
        out = self._process.stdout.readline().decode("utf-8").strip()
        self._lock.release()
        return out

    def __del__(self):
        self._process.kill()
