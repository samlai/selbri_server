import logging
from math import inf
from numpy import concatenate
from jboval import Jboval
from sklearn.metrics import accuracy_score  # type: ignore
from sklearn.model_selection import train_test_split  # type: ignore
from sklearn.neural_network import *  # type: ignore
from gensim.models import KeyedVectors  # type: ignore
import pickle
from typing import *
from utils import *
from os.path import exists
import sys

if __name__ == "__main__" and len(sys.argv) > 1 and sys.argv[1] == "train":
    from autosklearn.regression import *

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(message)s")

BPFK_wv_FILE = "data/bpfk.300.wordvectors"

wv = KeyedVectors.load(BPFK_wv_FILE, mmap='r')
# wv = KeyedVectors.load('jbo.wv', mmap='r')
pol_wv = KeyedVectors.load('gendata/pol.wv', mmap='r')


def _get_tanru_data():
    jboval = Jboval()
    data = []

    for w in (w for w in wv.index_to_key if jboval.vlalei(w) == "lujvo"):
        l = jboval.terjvo(w)
        if len(l) == 2:
            a, b = l
            if jboval.vlalei(a) == "gismu" and jboval.vlalei(b) == "gismu":
                data.append((w, a, b))
    return data


def tanru_data():
    tanru_data_filename = "./gendata/tanru_data.pkl"
    if exists(tanru_data_filename):
        with open(tanru_data_filename, "rb") as f:
            return pickle.load(f)
    else:
        data = _get_tanru_data()
        with open(tanru_data_filename, "wb") as f:
            pickle.dump(data, f)
        return data


def wv_tanru_data(data):
    X = []
    Y = []

    for w, a, b in data:
        X.append(concatenate((wv[a], wv[b])))
        Y.append(wv[w])
    return X, Y


def tanru_compile(m, seltau, tertau, topn=10):
    seltau = wv[seltau] if isinstance(seltau, str) else seltau
    tertau = wv[tertau] if isinstance(tertau, str) else tertau
    [v] = m.predict([concatenate((seltau, tertau))])
    return wv.similar_by_vector(v, topn=topn)


# Simple SUM:
# well predicted[train]: 0
# score[train]: 13.840476190476187
# well predicted: 0
# score: 6.230555555555554
# ['jbobau', 'jbopre', 'plixau', 'gendra', 'glibau', 'retsku', "pampe'o", "tinju'i", "mrobi'o", 'spusku', 'tcetce', 'kinzga', 'xrukla', "vokta'a", 'xamsku', "balcu'e", "cnima'o", 'mencre', 'sofybakni', 'voksnu', 'rarbau', 'rejgau', 'samtci', 'samymri', 'ricfoi', 'snavei', 'kargau', 'vrogai', "kansi'u", 'zvajbi', "pensi'u", "dunsi'u", "jupku'a", "klunu'i", 'nakpinji', 'dicysro', "ki'ogra", 'xunblabi', "ki'otre", 'sasfoi', "xajre'u", 'frejutsi']


def simple_lujvo_compile(seltau, tertau, topn=10):
    return wv.most_similar(positive=[seltau, tertau], topn=topn)


def lujvo_mean_reciprocal_rank(f, tests, topn=10):
    mrr = sum(max((1/(i+1)
                   for i, (wp, _) in enumerate(f(a, b, topn=topn))
                   if w == wp),
                  default=0)
              for w, a, b in tests)
    return mrr/len(tests) * 100


def lujvo_maximall_recall(f, tests):
    finded = sum(1
                 for w, a, b in tests
                 if w in map(fst, f(a, b)))
    return finded/len(tests) * 100


def load_model():
    with open("tanru_regresor_identity_1000000_bpfk_sgd.pkl", "rb") as f:
        return pickle.load(f)


# train_tanru_model()

class Unknown(Exception):
    pass


class TanruTranslate(object):
    def __init__(self):
        self.jboval = Jboval()
        self.m = load_model()

    def tanru_calc(self, l: Union[List[Any], str]):
        print(f"tanru_calc({l})")
        if type(l) is list:
            acc = self.tanru_calc(l[0])
            for x in l[1:]:
                ter = self.tanru_calc(x)
                [acc] = self.m.predict([concatenate((acc, ter))])
            return acc

        if l in wv:
            return wv[l]

        if self.jboval.vlalei(l) == "lujvo":
            return self.tanru_calc(self.jboval.terjvo(l))

        raise Unknown()

    def tanru_translate(self, l, topn=10):
        vec = self.tanru_calc(l)
        return pol_wv.most_similar(vec, topn=topn)


def test_function(f, tests, name="test"):
    e = lujvo_mean_reciprocal_rank(f, tests)
    q = lujvo_maximall_recall(f, tests)

    print(f"[{name}] Mean Reciprocal Rank: {e}")
    print(f"[{name}] Maximall Recall: {q}")


lujvo = "jvoka'a"

logging_config = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'custom': {
            # More format options are available in the official
            # `documentation <https://docs.python.org/3/howto/logging-cookbook.html>`_
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    },

    # Any INFO level msg will be printed to the console
    'handlers': {
        'console': {
            'level': 'INFO',
            'formatter': 'custom',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },

    'loggers': {
        '': {  # root logger
            'level': 'DEBUG',
        },
        'Client-EnsembleBuilder': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
    },
}

if __name__ == "__main__":
    print("Preparing data!")
    data = tanru_data()

    data_train, data_test = train_test_split(data, random_state=0)

    X_train, y_train = wv_tanru_data(data_train)
    X_test, y_test = wv_tanru_data(data_test)
    print(f"|train| = {len(data_train)}, |test| = {len(data_test)}")


if __name__ == "__main__" and len(sys.argv) > 1 and sys.argv[1] == "train":
    # m = MLPRegressor(random_state=1, max_iter=100000)

    m = MLPRegressor(random_state=42, activation="identity", tol=0.000000001, solver='sgd',
                     max_iter=100000, verbose=True, n_iter_no_change=100)

    # m = MLPRegressor(random_state=1, activation="identity",
    #                 max_iter=10000, alpha=0.00000001, solver="lbfgs")

    # m = MLPRegressor(random_state=1, activation="tanh", max_iter=10000)

    # m = MLPRegressor(random_state=1, activation="logistic", max_iter=10000)

    # m = AutoSklearnRegressor(logging_config=logging_config)

    print("Start training!")

    m.fit(X_train, y_train)
    print(f"[train] score: {m.score(X_train, y_train)}")
    print(f"[test] score: {m.score(X_test, y_test)}")

    if len(sys.argv) > 2:
        pkl_file = sys.argv[2]

        with open(pkl_file, "wb") as f:
            pickle.dump(m, f)

        sys.argv[1] = "test"

elif __name__ == "__main__" and len(sys.argv) > 1 and sys.argv[1] == "test_average":

    test_function(simple_lujvo_compile, data_train, "train")
    test_function(simple_lujvo_compile, data_test)

if __name__ == "__main__" and len(sys.argv) > 2 and sys.argv[1] == "test":
    with open(sys.argv[2], "rb") as f:
        m = pickle.load(f)

    def lujvo_predict(seltau, tertau, topn=10):
        seltau = wv[seltau] if isinstance(seltau, str) else seltau
        tertau = wv[tertau] if isinstance(tertau, str) else tertau
        [v] = m.predict([concatenate((seltau, tertau))])
        return wv.similar_by_vector(v, topn=topn)

    test_function(lujvo_predict, data_train, "train")
    test_function(lujvo_predict, data_test)
