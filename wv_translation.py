from typing import *
from utils import *
from gensim.models import Word2Vec, KeyedVectors  # type: ignore
from random import Random
import logging
import sys
import scoring

# def lojban_corpora():
#    return map(lambda s: map(prefix("jbo_"), s), BPFK_corpus().sents())


class Bridge_pol_jbo():
    def __init__(self,  name="jbo-pol.train.txt"):
        self.name = name
        self._len = None

    def generate_data(self, name):
        with open(name) as f:
            for l in f:
                w = l.strip().split(" ")
                jbo = w[0]

                for pol in ";".join(w[1:]).split(";"):
                    yield ["jbo_"+jbo, "pol_"+pol]
                    yield ["pol_"+pol, "jbo_"+jbo]

    def __len__(self):
        if self._len == None:
            self._len = sum(1 for x in self.generate_data(self.name))
        return self._len

    def __iter__(self):
        return self.generate_data(self.name)


#JBOPOL_model_FILE = "jbopol.model"
#JBOPOL_model_FILE = "/pio/data/corpuses/jbopol.model"
#JBOPOL_model_zabuzone_FILE = "zabuzane/jbopol_zabuzane_most50_0.25.model"
JBOPOL_model_zabuzone_FILE = "zabuzane/jbopol_zabuzane.model"
#JBOPOL_model_zabuzone_FILE = "/pio/data/corpuses/jbopol_zabuzane.model"
#JBOPOL_model_zabuzone_FILE = "/pio/data/corpuses/jbopol_zabuzane_mostrandom50_0.50.model"

#JBO_POL_BOOTSTRAP ="jbo2-pol.train.txt"

#path_polish_corpora = "data/polish_corpora.txt"
#path_polish_corpora = "/pio/data/corpuses/polish_corpora.txt"

#polish_model = "/pio/data/corpuses/polish.model"


logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


class Lojban_random_change():
    name = "BPFK_line_corpus.txt"

    def __init__(self, JBO_POL_BOOTSTRAP, prob, n=18, random=42):
        self.n = n
        self.random = random
        self._len = None
        self.prob = prob
        with open(JBO_POL_BOOTSTRAP) as f:
            self.jbopol = dict(l.strip().split() for l in f)

    def _change(self, word, random):
        if word in self.jbopol and random.random() < self.prob:
            return "pol_" + self.jbopol[word]
        else:
            return "jbo_" + word

    def __iter__(self):
        def gen():
            rand = Random(self.random)
            for _ in range(self.n):
                with open(self.name) as f:
                    for l in f:
                        yield list(self._change(word, rand)
                                   for word in l.strip().split())

        return gen()

    def __len__(self):
        with open(self.name) as f:
            return self.n * sum(1 for _ in f)


class Polish_random_change():
    name = path_polish_corpora

    def __init__(self, JBO_POL_BOOTSTRAP, prob, n=1, random=1):
        self.n = n
        self.random = random
        self._len = None
        self.prob = prob

        def swap(w):
            return w[1], w[0]

        with open(JBO_POL_BOOTSTRAP) as f:
            self.jbopol = dict(swap(l.strip().split()) for l in f)

    def _change(self, word, random):
        if word in self.jbopol and random.random() < self.prob:
            return "jbo_" + self.jbopol[word]
        else:
            return "pol_" + word

    def __iter__(self):
        def gen():
            rand = Random(self.random)
            for _ in range(self.n):
                with open(self.name) as f:
                    for l in f:
                        yield list(self._change(word, rand)
                                   for word in l.strip().split())

        return gen()

    def __len__(self):
        with open(self.name) as f:
            return self.n * sum(1 for _ in f)


def get_data():
    return RandMerge(It(line_corpora, [path_polish_corpora, "pol_"]),
                     Multi(Bridge_pol_jbo(), 10005),
                     Multi(It(line_corpora, ["BPFK_line_corpus.txt", "jbo_"]), 18))


def get_data2(JBO_POL_BOOTSTRAP, prob):
    return RandMerge(Polish_random_change(JBO_POL_BOOTSTRAP, prob),
                     Lojban_random_change(JBO_POL_BOOTSTRAP, prob))


def get_polish_corpora():
    return File(path_polish_corpora)


def train_model(data, file_name, epochs):
    print("Gensim Start!")
    # Creating Word2Vec
    model = Word2Vec(sentences=data,
                     vector_size=300,
                     window=10,
                     workers=7,
                     epochs=epochs)
    return model

    print("Saving!")
    model.save(file_name)


def load_KV(name):
    return KeyedVectors.load(name, mmap='r')


def load_model(name):
    return Word2Vec.load(name, mmap='r')


def load_model1():
    return Word2Vec.load(JBOPOL_model_FILE, mmap='r')


def load_model2():
    return Word2Vec.load(JBOPOL_model_zabuzone_FILE, mmap='r')


#print("Preparing Data!")
#data = get_data2()
#file_name = JBOPOL_model_zabuzone_FILE
#jbo_pol_wv = train_model(data, file_name)

def jbopol_translate(wv, w, topn=10):
    return list(map(lambda w: (w[0][4:], w[1]),
                    firstn(((x, d)
                            for x, d in wv.most_similar("jbo_"+w, topn=20)
                            if x.startswith("pol_")),
                           topn)))


def boot_dict(jbo, pol, f, out):
    with open(out, "w") as fs:
        for w in jbo.key_to_index:
            [(trn, dist)] = pol.most_similar([jbo[w]], topn=1)
            if dist > f:
                fs.write(f"{w} {trn}\n")

#jbo_pol_wv = train_model()
#jbo_pol_wv = load_model2()

#jbo_pol_wv = train_model(File_Iterator(""), file_name)
jbo_pol_wv = load_model2()


def wv_translation(wv, w, topn=10):
    return list(map(lambda w: (w[0][4:], w[1]),
                    firstn(((x, d)
                            for x, d in wv.most_similar("jbo_"+w, topn=100000)
                            if x.startswith("pol_")),
                           topn)))


def wv_rewerse(w, topn=10):
    return list(map(lambda w: (w[0][4:], w[1]),
                    firstn(((x, d)
                            for x, d in jbo_pol_wv.wv.most_similar("pol_"+w, topn=100000)
                            if x.startswith("jbo_")),
                           topn)))


def split_jbo_pol(wv, jbo_wv, pol_wv):
    pol_k = []
    pol_v = []
    jbo_k = []
    jbo_v = []
    for word in wv.key_to_index:
        ty, key = word.split("_", 1)
        if ty == "pol":
            pol_k.append(key)
            pol_v.append(wv[word])
        elif ty == "jbo":
            jbo_k.append(key)
            jbo_v.append(wv[word])
        else:
            print(f"Unknown word: {word}")
    jbo = KeyedVectors(300)
    jbo.add_vectors(jbo_k, jbo_v)
    jbo.save(jbo_wv)

    pol = KeyedVectors(300)
    pol.add_vectors(pol_k, pol_v)
    pol.save(pol_wv)


def nearest_tebele(word):
    # Finding most similar words
    print("3 words similar to karce")
    words = jbo_pol_wv.wv.most_similar(word, topn=10)
    print("""\\begin{center}
            \\begin{tabular}{ c | c | c }
Podobieństwo & słowo & ręczne tłumaczenie \\\\
        \\hline""")
    for word, sim in words:
        print(f"{sim} & \jbo{{{word}}} & \pol{{}}\\\\")
    print("""\\end{tabular}
            \\end{center}""")

def latex(call):
    tex = """\\begin{figure}[h!]
\\begin{center}
\\begin{tabular}{ c | c | c}
 szablon & Precision & Maximall Recall \\\\
 \\hline
 most50 & %f\\%% & %d\\%% \\\\
 random50 & %f\\%% & %d\\%% \\\\
\\end{tabular}
\\caption{Ocena programu %s}
\\label{fig:%s}
\\end{center}
\\end{figure}"""
    def f(x):
        return list(map(lambda c: c[0], call(x)))

    most50 = scoring.scoring_method(f, "data/most50.csv")
    most50_max = scoring.max_scoring_method(f, "data/most50.csv")
    rand50 = scoring.scoring_method(f, "data/random50.csv")
    rand50_max = scoring.max_scoring_method(f, "data/random50.csv")
    print(tex % (most50 * 2, most50_max * 2, rand50 * 2, rand50_max * 2,
                 call.__name__.replace("_", "\\_"), call.__name__))


if __name__ == '__main__' and len(sys.argv) > 1:
    if sys.argv[1] == "train":
        prob = float(sys.argv[2])
        epohs = int(sys.argv[3])
        JBO_POL_BOOTSTRAP = "/pio/data/corpuses/boot/" + sys.argv[4]
        name = "/pio/data/corpuses/boot/" + sys.argv[5]

        print("Preparing Data!")
        data = get_data2(JBO_POL_BOOTSTRAP, prob)

        print("Gensim Start!")
        model = train_model(data, epohs)

        print("Saving!")
        model.save(name)

    elif sys.argv[1] == "boot":
        jbo_name = "/pio/data/corpuses/boot/" + sys.argv[2]
        jbo = load_KV(jbo_name)

        pol_name = "/pio/data/corpuses/boot/" + sys.argv[3]
        pol = load_KV(pol_name)

        f = float(sys.argv[4])

        out = "/pio/data/corpuses/boot/" + sys.argv[5]

        boot_dict(jbo, pol, f, out)

    elif sys.argv[1] == "split":
        name = "/pio/data/corpuses/boot/" + sys.argv[2]
        model = load_model(name)

        jbo_name = "/pio/data/corpuses/boot/" + sys.argv[3]
        pol_name = "/pio/data/corpuses/boot/" + sys.argv[4]

        split_jbo_pol(model.wv, jbo_name, pol_name)
    elif sys.argv[1] == "test_jbopol":
        name = "/pio/data/corpuses/boot/" + sys.argv[2]
    
        model = load_model(name)
    
        latex(lambda x: wv_translation(model.wv, x))
    
    else:
        sys.exit(1)
