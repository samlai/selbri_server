from typing import *
from utils import *
import regex as re  # type: ignore
from math import inf


def score_translation(output: Union[Iterator[str], List[str]], model: List[List[str]]) -> Tuple[float, str]:
    if output == []:
        return 0.0, "[]"
    for i, w in enumerate(output):

        s = min((j for j in range(0, len(model))
                 if w in model[j]),
                default=inf)

        if s != inf:
            return 1/((s+1)*(i+1)), w
    return 0.0, "---"


def scoring_method(method: Callable[[str], List[str]], benchmark_filename: str = "data/most50.csv") -> float:
    with open(benchmark_filename) as f:
        score = 0.0
        n = 0
        for l in f:
            m3 = re.match("^(\w+)\t(.+)$", l)
            lojban = m3.group(1)
            model = list(map(lambda w: w.split(";"), m3.group(2).split("\t")))

            output = method(lojban)

            o, w = score_translation(output, model)
            score += o
            n += 1
            # if o == 0.0:
            #    print(f"{lojban}\t{o:.4}\t{w}\t{output}")
            # print(f"{lojban}\t{o:.4}\t{w}")

        return score/n


def max_scoring_method(method: Callable[[str], List[str]], benchmark_filename: str = "data/most50.csv") -> float:
    with open(benchmark_filename) as f:
        score = 0
        n = 0
        for l in f:
            lojban, model_txt = l.strip().split("\t", 1)

            model = set(w
                        for rec in model_txt.split("\t")
                        for w in rec.split(";"))

            output = set(method(lojban))

            score += len(output & model) > 0
            n += 1
            # if o == 0:
            #    print(f"{lojban}\t{output}")

        return score/n


def scoring_methods(methods: List[Callable[[str], List[Tuple[float, Tuple[str, str]]]]], benchmark_filename: str = "data/most50.csv") -> List[float]:
    with open(benchmark_filename) as f:
        scores: Iterator[float] = (x for x in [0.0] * len(methods))
        for l in f:
            m3 = re.match("^(\w+)\t(.+)$", l)
            lojban = m3.group(1)
            model = list(map(lambda w: w.split(";"), m3.group(2).split("\t")))
            outputs = map(lambda f: map(fst, map(snd, f(lojban))), methods)
            l_o_w = list(
                map(lambda out: score_translation(out, model), outputs))
            scores = map((lambda w: w[0]+w[1]), zip(scores, map(fst, l_o_w)))
            s = " ".join(map(lambda p: f"{p[0]:<.5f}  {p[1]:13}", l_o_w))
            print(f"{lojban}\t{s}")
        return list(scores)


def latex(call):
    tex = """\\begin{center}
\\begin{tabular}{ c | c | c}
  & Mean reciprocal rank & Maximall Recall \\\\
 \\hline
 most50 & %f%% & %f%% \\\\
 random50 & %f%% & %f%% \\\\
\\end{tabular}
\\end{center}"""
    most50 = scoring_method(call)
    most50_max = max_scoring_method(call)
    rand50 = scoring_method(call, "data/random50.csv")
    rand50_max = max_scoring_method(call, "data/random50.csv")
    print(tex % (10*most50, 10*most50_max, 10*rand50, 10*rand50_max))
