import regex as re  # type: ignore


def load_mublin_eng():
    dictionary = dict()
    with open("data/ralju/jbo-eng_mublin-etymology.dsl", "r") as f:
        t = None
        for line in f:
            if (line[0] == "#"):
                continue
            m = re.search("^\s+(\w*)\s*:\s*(\w*)", line)  # type: ignore
            if m is not None:
                t[m.group(1)] = m.group(2)
                continue
            m = re.search("^(\w+)", line)
            if m is not None:
                t = dict()
                tag = m.group(1)
                if tag in dictionary:
                    dictionary[tag].append(t)
                else:
                    dictionary[tag] = [t]
        return dictionary
