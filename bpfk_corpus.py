import string
from subprocess import Popen, PIPE
#from matplotlib import pyplot
# from sklearn.decomposition import PCA  # type: ignore
from gensim.models import Word2Vec, KeyedVectors, FastText  # type: ignore
from nltk.corpus import brown  # type: ignore
import nltk  # type: ignore
import regex as re  # type: ignore
from typing import *
from itertools import chain
from utils import *
from jboval import Jboval
import sys

BPFK_wv_FILE = "data/bpfk.300.wordvectors"
BPFK_ft_FILE = "data/bpfk.300.fasttext.gensim"


class Tokenizer_camxes:
    p_STOP = "==STOP==\n"

    def __init__(self):
        self._process = Popen(["/home/samuel/Dokumenty/UWr/inżynierka/tokenizer_camxes.js"],
                stdin=PIPE,
                stdout=PIPE
                )

    def tokenize(self, s: str) -> Iterable[Tuple[str, str]]:
        s = s.replace("\n", " ")
        self._process.stdin.write(f"{s}\n".encode("utf-8"))
        self._process.stdin.flush()

        while True:
            x = self._process.stdout.readline().decode("utf-8")
            if x == self.p_STOP:
                return
            else:
                yield eval(x)

    def __del__(self):
        self._process.terminate()


class BPFK_corpus:
    file_name = "data/lojban_corpus.txt"
    p_start = re.compile("^-start-+$")
    p_end = re.compile("^-end-+$")
    p_i = re.compile(r"[^a-zA-Z']i|\n\n\s*")
    p_paragraf_separator = re.compile(r"\n\s*\n")

    def __init__(self):
        self.jboval = Jboval()
        self.tokenizer = Tokenizer_camxes()


    def words_types(self) -> List[Tuple[str, str]]:
        return [word
                for paras in self.paras_raw()
                for sent in self.paras_to_sents(paras)
                for word in sent]

    def sents(self) -> List[List[str]]:
        return list(self.sents_gen())

    def sents_gen(self) -> Iterator[List[str]]:
        return (list(map(snd, sent))
                for sent in self.sents_types())

    def sents_types(self) -> Iterator[List[Tuple[str, str]]]:
        return (sent
                for paras in self.paras_raw()
                for sent in self.paras_to_sents(paras))

    def paras_to_sents(self, paras: str) -> Iterator[List[Tuple[str, str]]]:

        for paragraf in self.p_paragraf_separator.split(paras):
            sentence: List[Tuple[str, str]] = []

            for piece in paragraf.split(" "):
                for w in self.tokenizer.tokenize(piece):
                    if w == ['cmavo', 'i']:
                        if sentence != [] and sentence[-1][0] == "drata":
                            sentence = sentence[:-1]
                        if sentence != []:
                            yield sentence
                        sentence = []
                        continue
                    [t, s] = w

                    if t == "lujvo":
                        s = self.jboval.rafyjongau(s.split("-"))

                    sentence.append((t, s))

            if sentence != []:
                yield sentence

    def paras_types(self) -> Iterator[List[List[Tuple[str, str]]]]:
        return (list(self.paras_to_sents(paras))
                for paras in self.paras_raw())

    def paras_raw(self) -> Iterator[str]:
        phrase: Optional[str] = None
        with open(self.file_name) as f:
            for l in f:
                if self.p_start.match(l):
                    phrase = ""
                elif self.p_end.match(l) and phrase is not None:
                    yield phrase
                    phrase = None
                elif phrase is not None:
                    phrase += l


def train_model():
    print("Start traning!")
    data = It(line_corpora, ["BPFK_line_corpus.txt"])
    # Creating Word2Vec
    model = Word2Vec(
            sentences=data,
            vector_size=300,
            window=10,
            workers=4,
            epochs=40
            )

    word_vectors = model.wv
    word_vectors.save(BPFK_wv_FILE)

def train_model_fast_test():
    print("Start traning!")
    data = It(line_corpora, ["BPFK_line_corpus.txt"])
    # Creating FastText
    model = FastText(
            sentences=data,
            vector_size=300,
            window=10,
            workers=4,
            epochs=40
            )

    word_vectors = model.wv
    word_vectors.save(BPFK_ft_FILE)

def bpfk_load_model():
    return KeyedVectors.load(BPFK_wv_FILE, mmap='r')


def save_to_muse_format(wv):
    print(f"{len(wv)} {wv.vector_size}")
    for w in wv.index_to_key:
        print(f"{w} {' '.join(map(repr,list(wv[w])))}")


def nearest_tebele(word):
    # Finding most similar words
    print("3 words similar to karce")
    words = wv.most_similar(word, topn=10)
    print("""\\begin{center}
            \\begin{tabular}{ c | c | c }
Podobieństwo & słowo & ręczne tłumaczenie \\\\
        \\hline""")
    for word, sim in words:
        print(f"{sim} & \jbo{{{word}}} & \pol{{}}\\\\")
    print("""\\end{tabular}
            \\end{center}""")

#wv = train_model_fast_test()
#save_to_muse_format(wv)


# if __name__ == "__main__":
#     nearest_tebele("prami")
#
#     # Visualizing data
#     words = ["fasygu'e", "dotygu'e", "bevma'e",
#              "bloti", "dargu", "terctu", "ecre"]
#
#     X = wv[words]
#     pca = PCA(n_components=2)
#     result = pca.fit_transform(X)
#
#     pyplot.scatter(result[:, 0], result[:, 1])
#     for i, word in enumerate(words):
#         pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))  # type: ignore
#     pyplot.show()

if __name__ == '__main__':
    if not sys.flags.inspect:
        if len(sys.argv) > 1 and sys.argv[1] == '--train':
            train_model()
    else:
        wv = bpfk_load_model()

#print("\a")
