#!/usr/bin/python3

import socket
import sys
import os
import signal
from typing import *
from utils import *
from itertools import count
from math import inf
from importlib import reload
from wnet_translation import *
from wv_translation import wv_translation
import json
import regex as re  # type: ignore
from tanru import TanruTranslate, wv as jbo_wv
import threading
from morfeusz2 import Morfeusz
import traceback

make = """import selbri_server;
reload(selbri_server);
from selbri_server import *"""

tt = TanruTranslate()

rzeczownik = {"subst", "depr", "ppron12", "ppron3"}

czasownik = {"fin", "bedzie", "aglt", "praet", "impt",
             "imps", "inf", "pcon", "pant", "ger", "pact", "ppas", "winien"}

przymiotnik = {"adj", "adja", "adjp", "adjc"}

liczebnik = {"num", "numcol"}


def wnet_ty_from_mor(ty):
    fty = ty.split(":")[0]
    if fty in rzeczownik:
        return "rzeczownik"
    elif fty in czasownik:
        return "czasownik"
    elif fty in przymiotnik:
        return "przymiotnik"
    else:
        return "xxx"


jbo_words = set(jbo_wv.key_to_index)


class Client_Service():
    def __init__(self, client, address):
        self.thread = threading.Thread(
            target=self._run_client, args=(client, address), daemon=True)
        self.mor = Morfeusz()
        self.comands = {"wnet": wnet_translation,
                        "wv": wv_translation,
                        "class": type_classyfication,
                        #"analyse": self.mor.analyse,
                        #"generate": self.mor.generate,
                        "tanru": self.tanru_translation_fin,
                        "most_similar": self.find_most_similar}

    def find_most_similar(self, d):
        data = json.loads(d)
        return jbo_wv.most_similar_to_given(data[0], list(jbo_words & set(data[1])))

    def tanru_translation_fin(self, d):
        wv_data = tt.tanru_translate(json.loads(d))
        print(wv_data)
        return list(set((fi, wnet_ty_from_mor(ty))
                        for x, _ in wv_data
                        for _, _, (_, fi, ty, _, _) in self.mor.analyse(x)))

    def _client_service(self, fd):
        while True:
            line = fd.readline().strip()
            if not line:
                print("Break because of empty line!")
                break
            print(f"Line: {line}")
            com, data = line.split(" ", 1)

            try:
                if com in self.comands:
                    out = json.dumps(self.comands[com](data))
                else:
                    out = "fail"
            except Exception as e:
                print(f"Fail: {e}")
                print(e)
                out = "fail"

            print(f"Out: {out}")
            sys.stdout.flush()
            fd.write(out + "\n")
            fd.flush()

    def _run_client(self, client, address):
        print(f"Start service for: {address}")
        fd = client.makefile(mode="rw")
        try:
            self._client_service(fd)
        except Exception as e:
            print(f"exception: {e}")
            traceback.print_exception(e)
        finally:
            client.close()

    def start(self):
        self.thread.start()

    def __del__(self):
        del self.mor


def start_server():
    signal.signal(signal.SIGCHLD, signal.SIG_IGN)

    with socket.socket() as server:
        server.bind((os.environ["SELBRI_HOST"], int(os.environ["SELBRI_PORT"])))
        server.listen()
        print("Start Server!")

        while True:
            try:
                Client_Service(*server.accept()).start()
            except KeyboardInterrupt:
                break

    print("Stop Server")


# if __name__ == '__main__':
#    if not sys.flags.inspect:
#        start_server()

start_server()
# print("\a")
