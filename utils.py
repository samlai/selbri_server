from random import Random
from typing import *


def prefix(p: str):
    return lambda w: p + w


def line_corpora(name, pre=""):
    with open(name) as f:
        for l in f:
            yield list(map(prefix(pre), l.strip().split()))


def ave(l):
    n = 0
    s = 0
    for x in l:
        s += x
        n += 1
    return s / n


def fst(w):
    return w[0]


def remove_repetitions(gen, key=lambda x: x):
    s = set()
    for x in gen:
        k = key(x)
        if k in s:
            continue
        else:
            s.add(k)
            yield x


def mix_fst(l: Iterator[Tuple[float, Any]]):
    c: Dict[float, Any] = dict()
    for k, w in l:
        if w in c:
            c[w] = min(c[w], k)
        else:
            c[w] = k
    return map(lambda w: (w[1], w[0]), c.items())


def snd(w):
    return w[1]


def head(gen):
    for x in gen:
        return x


def firstn(gen, n):
    return map(snd, zip(range(n), gen))


class File_Iterator(object):
    def __init__(self, name):
        self.name = name
        self._len = None

    def __iter__(self):
        with open(self.name) as f:
            for l in f:
                yield l.strip().split()

    def __len__(self):
        with open(self.name) as f:
            return sum(1 for _ in f)


class It(object):
    def __init__(self, fun, args=[]):
        self.fun = fun
        self.args = args
        self._len = None

    def __iter__(self):
        return self.fun(*self.args)

    def __len__(self):
        if self._len == None:
            self._len = sum(1 for x in self.fun(*self.args))
        return self._len


class Chain(object):
    def __init__(self, *it):
        self.it = it

    def __iter__(self):
        def iterator():
            for i in self.it:
                for x in i:
                    yield x
        return iterator()

    def __len__(self):
        if self._len == None:
            self._len = sum(len(i) for i in self.it)
        return self._len


class Multi():
    def __init__(self, it, n):
        self.it = it
        self.n = n

    def __iter__(self):
        return (x
                for _ in range(self.n)
                for x in self.it)

    def __len__(self):
        return len(self.it) * self.n


class RandMerge(object):
    def __init__(self, *it, random=42):
        self.it = it
        self.random = random
        self._len = None

    def __iter__(self):
        def iterator():
            rand = Random(self.random)
            iters = list(map(iter, self.it))

            while iters != []:
                i = rand.randrange(len(iters))
                try:
                    yield iters[i].__next__()
                except StopIteration:
                    iters.pop(i)

        return iterator()

    def __len__(self):
        if self._len == None:
            self._len = sum(map(len, self.it))
        return self._len
