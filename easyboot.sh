#!/bin/bash

# Usage: easyboot.sh 5 90

nohup bash -x bootstrap.sh epoch_$1_$2 "0.$2" $1 > epoch_$1_$2.log &
