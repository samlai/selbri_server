import pandas

polimorf = pandas.read_csv("data/polimorf-20210829.tab",
                           sep="\t",
                           dtype="string",
                           engine='python',
                           header=0,
                           index_col=False,
                           low_memory=True,
                           memory_map=True)
