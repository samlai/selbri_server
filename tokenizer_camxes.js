#!/usr/bin/env node

//module.exports = camxes
// load peg.js and the file system module
const util = require('util')
const readline = require('readline');
const PEG = require("pegjs")
const fs = require("fs")
// read peg and build a parser
var camxes = PEG.generate(fs.readFileSync("cmaxes_ilmen.js.peg").toString(), {
	cache: true
});

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: false
});

console.flog = (myObject) => {
	console.log(util.inspect(myObject, {
		showHidden: false,
		depth: null,
		colors: true
	}))
}

function get_word(w) {
	if (typeof(w) == "object") {
		return get_word(w[1])
	} else if (typeof(w) == "string") {
		return w
	}
}

/*
function _concat(xs) {
	acc = []
	for (const x of xs) {
		acc = acc.concat(x)
	}
	console.error("Udało się!")
	return acc
}
*/

rl.on('line', function(line) {
	//console.log("==START==")
	for (const token of line.toLowerCase().split(" ")) {
		try {
			l = camxes.parse(token);
		} catch (e) {
			l = [
				["non_lojban", token]
			];
		}
		for (const w of l) {
			console.log("%s", JSON.stringify(w))
		}
	}
	console.log("==STOP==")
})
