from nltk.tokenize import RegexpTokenizer  # type: ignore
from nltk.probability import FreqDist  # type: ignore

tokenizer = RegexpTokenizer("[\w']+")


def compute_lojban_1_grams():
    fdist = FreqDist()
    with open("data/lojban_corpus.txt") as f:
        for word in tokenizer.tokenize(f.read()):
            fdist[word.lower()] += 1
    return fdist


def save_lojban_1_grams(fr):
    with open("data/lojban_1grams", "w") as f:
        for w in fr:
            f.write(f"{fr[w]}\t{w}\n")


def get_gimsus():
    with open("data/gismu_list.csv") as f:
        return (l[:-1] for l in f)


def most_used_gismu(fr):
    return sorted(((fr[g], g) for g in get_gimsus()), reverse=True)
